#include "WhoAmI.hxx"

namespace coreutils
{

  WhoAmI::WhoAmI()
  {
    uid = getuid();
    gid = getgid();
  }

  WhoAmI::~WhoAmI()
  {
  }

  std::string WhoAmI::getUserName() const
  {
    return getpwuid(uid)->pw_name;
  }

  std::string WhoAmI::getUserPassword() const
  {
    return getpwuid(uid)->pw_passwd;
  }

  std::string WhoAmI::getGroupName() const
  {
    return getgrgid(gid)->gr_name;
  }

  std::string WhoAmI::getGroupPassword() const
  {
    return getgrgid(gid)->gr_passwd;
  }

  std::vector<std::string> WhoAmI::getGroupMembers() const
  {
    std::vector<std::string> members;
    for (auto m = getgrgid(gid)->gr_mem; *m; ++m) {
      members.push_back(*m);
    }
  }

  std::string WhoAmI::getGecos() const
  {
    return getpwuid(uid)->pw_gecos;
  }

  std::string WhoAmI::getHomeDirectory() const
  {
    return getpwuid(uid)->pw_dir;
  }

  std::string WhoAmI::getShell() const
  {
    return getpwuid(uid)->pw_shell;
  }

  unsigned WhoAmI::getUID() const
  {
    return uid;
  }

  unsigned WhoAmI::getGID() const
  {
    return gid;
  }

}
