#include "main.hxx"
#include <string>
#include <vector>

int main(const int argc, const char *argv[])
{
    coreutils::WhoAmI whoami;
    std::cout << whoami.getUserName() << std::endl;

    return EXIT_SUCCESS;
}