#ifndef COREUTILS_WHO_AM_I_HXX
#define COREUTILS_WHO_AM_I_HXX

#include <string>
#include <vector>
#include <unistd.h>
#include <grp.h>
#include <pwd.h>
#include <sys/types.h>

namespace coreutils
{
  class WhoAmI
  {
  public:
    WhoAmI();
    ~WhoAmI();
    std::string getUserName() const;
    std::string getUserPassword() const;
    std::string getGroupName() const;
    std::string getGroupPassword() const;
    std::vector<std::string> getGroupMembers() const;
    std::string getGecos() const;
    std::string getHomeDirectory() const;
    std::string getShell() const;
    unsigned getUID() const;
    unsigned getGID() const;
  private:
    unsigned uid;
    unsigned gid;
  };
}

#endif //COREUTILS_WHO_AM_I_HXX
