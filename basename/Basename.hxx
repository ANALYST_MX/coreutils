#ifndef COREUTILS_BASENAME_HXX
#define COREUTILS_BASENAME_HXX

#include <iostream>
#include <string>

namespace coreutils
{
  class Basename
  {
  public:
    Basename(const std::string & = "", const std::string & = "");
    ~Basename();
    const std::string getPath() const;
    const std::string getName() const;
    const std::string getSuffix() const;
    void setPath(const std::string &);
    void setSuffix(const std::string &);
  protected:
    const std::string ignoreSuffix(const std::string &, const std::string &) const;
    const std::string ignorePath(const std::string &) const;
    const char getDirectorySeparator() const;
    const std::string treatLastMultipleDirectorySeparator(const std::string &) const;
  private:
    std::string path;
    std::string suffix;
  };
}

#endif //COREUTILS_BASENAME_HXX
