#include "Basename.hxx"

namespace coreutils
{
  Basename::Basename(const std::string &path, const std::string &suffix)
    : path(path), suffix(suffix)
  {
  }
  
  Basename::~Basename()
  {
  }

  void Basename::setPath(const std::string &newPath)
  {
    path = newPath;
  }
  
  void Basename::setSuffix(const std::string &newSuffix)
  {
    suffix = newSuffix;
  }

  const std::string Basename::getPath() const
  {
    return path;
  }
  
  const std::string Basename::getName() const
  {
    if ( path.empty() || path.length() == 1)
      {
	return path;
      }
    
    return ignorePath(ignoreSuffix(path, suffix));
  }
  
  const std::string Basename::getSuffix() const
  {
    return suffix;
  }

  const std::string Basename::ignoreSuffix(const std::string &path, const std::string &suffix) const
  {
    if ( path.empty() || suffix.empty() || path.length() == 1 )
      {
	return path; 
      }
    
    std::size_t found = path.rfind(suffix);
    if ( std::string::npos == found || (path.length() - suffix.length()) != found )
      {
	return path;
      }
    else if ( found > 0 && path.at(found - 1) == getDirectorySeparator() )
      {
	return suffix;
      }

    return path.substr(0, found);
  }

  const std::string Basename::ignorePath(const std::string &path) const
  {
    std::string res;
    if ( path.empty() || path.length() == 1 )
      {
	return path;
      }

    res = treatLastMultipleDirectorySeparator(path);
    std::size_t found = res.rfind(getDirectorySeparator());
    if ( std::string::npos == found )
      {
	return res;
      }
    
    return res.substr(found + 1);
  }

  const std::string Basename::treatLastMultipleDirectorySeparator(const std::string &path) const
  {
    std::size_t i = path.length() - 1;
    for (; i > 0 && path.at(i) == getDirectorySeparator(); --i )
      {
      }

    return path.substr(0, i + 1);
  }

  const char Basename::getDirectorySeparator() const
  {
#if defined(_WIN32) || defined(__MSDOS__) || defined(__CYGWIN__)
    return '\\';
#else
    return '/';
#endif
  }
  
}
