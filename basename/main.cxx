#include "main.hxx"

int main(const int argc, const char *argv[])
{
  if ( argc < 2 || argc > 3 )
    {
      return EXIT_SUCCESS;
    }
  
  coreutils::Basename basename;
  if ( argc == 2 )
    {
      basename.setPath(argv[1]);
    }
  else if ( argc == 3 )
    {
      basename.setPath(argv[1]);
      basename.setSuffix(argv[2]);
    }

  std::cout << basename.getName() << std::endl;
  
  return EXIT_SUCCESS;
}
