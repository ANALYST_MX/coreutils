#ifndef COREUTILS_TRUE_HXX
#define COREUTILS_TRUE_HXX

namespace coreutils
{
    class True
    {
    public:
        True();
        ~True();
        const bool getValue() const;

    private:
        const bool value;
    };
}

#endif //COREUTILS_TRUE_HXX
