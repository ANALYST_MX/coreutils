#include "True.hxx"

namespace coreutils
{
    True::True()
        : value(true)
    { }

    True::~True()
    { }

    const bool True::getValue() const
    {
        return value;
    }
}