#include "Who.hxx"

namespace coreutils
{
  Who::Who()
    : UTMPPath("/var/run/utmp")
  {
  }

  Who::~Who()
  {
  }

  void Who::setUTMPPath(const std::string &newPath)
  {
    UTMPPath = newPath;
  }
}
