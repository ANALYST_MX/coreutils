#ifndef COREUTILS_WHO_HXX
#define COREUTILS_WHO_HXX

#include <fstream>
#include <string>
#include <utmp.h>

namespace coreutils
{
  class Who
  {
  public:
    Who();
    ~Who();
    void setUTMPPath(const std::string &);
  private:
    std::string UTMPPath;
  };
}

#endif //COREUTILS_WHO_HXX
