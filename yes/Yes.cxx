#include "Yes.hxx"

namespace coreutils
{
    Yes::Yes(const std::string &newValue)
        : value(newValue)
    {
    }

    Yes::~Yes()
    {
    }

    const std::string Yes::getValue() const
    {
        return value;
    }
}