#ifndef COREUTILS_YES_H
#define COREUTILS_YES_H

#include <string>

namespace coreutils
{
    class Yes
    {
    public:
        Yes(const std::string & = "y");
        ~Yes();
        const std::string getValue() const;

    private:
        const std::string value;
    };
}

#endif //COREUTILS_YES_H
