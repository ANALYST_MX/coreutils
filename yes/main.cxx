#include "main.hxx"
#include "Yes.hxx"

int main(const int argc, const char *argv[])
{
    std::string value;
    if ( argc < 2 )
    {
        value = coreutils::Yes().getValue();
    }
    else
    {
        for ( auto i = 1; i < argc; ++i )
        {
            value += argv[i];
        }
        value = coreutils::Yes(const_cast<const std::string &>(value)).getValue();
    }

    while ( true )
    {
        std::cout << value << std::endl;
    }

    return EXIT_SUCCESS;
}
